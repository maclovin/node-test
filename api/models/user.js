var mongoose = require('mongoose'),
    Schema = mongoose.Schema

var UserSchema = new Schema({
        linkedin: String,
        email : String,
        password : String,
        firstName : String,
        lastName : String,
        company : String,
        category : Array,
        address : String,
        city : String,
        state : String,
        zipcode : Number,
        phone : Number,
        lastAccess : Date
})

module.exports = mongoose.model('Users', UserSchema);
