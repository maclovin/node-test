var express = require('express'),
    config = require('./config'),
    app = express(),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    sqlinjection = require('sql-injection'),
    passport = require('passport'),
    session = require('express-session'),
    mongoose = require('mongoose'),
    port = process.env.PORT || config.serverPort

mongoose.connect(config.database.host + config.database.name)
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(session({
    secret : config.sessionSecret,
    resave: true,
    saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())

app.use(require('./controllers'))

app.listen(port, function() {
    console.log('Listening on port ' + port)
})
