var passport = require('passport'),
    User = require('../models/user')

var UserSession = function() {
    passport.serializeUser(function(user, done) {
        done(null, user.id)
    })
    passport.deserializeUser(function(id, done) {
        User.findById(id, function (err, user) {
            done(err, user)
        })
    })
}

module.exports = UserSession
