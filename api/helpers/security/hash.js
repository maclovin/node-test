var bCrypt = require('bcrypt'),
    config = require('../../config')

Hash = {
    generate : function(content) {
        var salt = bCrypt.genSaltSync(10)
            if (content)
                return bCrypt.hashSync(content, salt)
            return false
    },
    compare : function(content, hash) {
        if (content && hash)
            return bCrypt.compareSync(content, hash)
        return false
    }
}

module.exports = Hash;
