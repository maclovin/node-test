var joi = require('joi');

var Schemas = {
    user: joi.object().keys({
        linkedin: joi.string(),
        email: joi.string().email().required(),
        firstName: joi.string().required(),
        lastName: joi.string().required(),
        password : joi.string().required(),
        firstName : joi.string().required(),
        lastName : joi.string().required(),
        company : joi.string().required(),
        category : joi.string().required(),
        address : joi.string().required(),
        city : joi.string().required(),
        state : joi.string().required(),
        zipcode : joi.string().required(),
        phone : joi.string().required()
    })
 }

 module.exports = Schemas
