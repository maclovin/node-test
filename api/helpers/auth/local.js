var passport = require('passport'),
    config = require('../../config'),
    init = require('./init'),
    LocalStrategy = require('passport-local'),
    User = require('../../models/user'),
    Hash = require('../security/hash')

passport.use(new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password'
    },
    function(email, password, done) {
        var searchQuery = {
            email : email
        }

        User.findOne(searchQuery, function(err, user) {
            if (err || !user)
                return done(err)
            if (!Hash.compare(password, user.password))
                return done(err)

            return done(null, user)
        })
    }
))

init()

module.exports = passport
