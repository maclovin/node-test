var passport = require('passport'),
    User = require('../../models/user')

var UserSession = function() {
    passport.serializeUser(function(user, done) {
        console.log('SERIALIZING: ', user.id)
        done(null, user.id)
    })
    passport.deserializeUser(function(id, done) {
        console.log('DESERIALIZE:', id)

        User.findById(id, function (err, user) {
            done(err, user)
        })
    })
}

module.exports = UserSession
