var passport = require('passport'),
    config = require('../../config'),
    init = require('./init'),
    LinkedInStrategy = require('passport-linkedin'),
    User = require('../../models/user')

passport.use(new LinkedInStrategy({
        consumerKey : config.linkedin.id,
        consumerSecret : config.linkedin.secret,
        callbackURL : config.linkedin.callback,
        profileFields : ['id', 'first-name', 'last-name', 'email-address', 'headline', 'specialties', 'location']
    },
    function(token, tokenSecret, profile, done) {
        var searchQuery = {
            name : profile.emailAddress
        }
        var updates = {
            linkedin : profile.id
        }
        var options = {
            upsert : true
        }
        User.findOneAndUpdate(searchQuery, updates, options, function(err, user) {
            if(err) 
                return done(err)

            return done(null, user)
        })
    }
))

init()

module.exports = passport
