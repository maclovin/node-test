var config = {
    sessionSecret : 'keyboard cat',
    hashSalt : 'b0l0l0h4hAAA',
    serverPort : '3000',
    database : {
        name : 'nodetest',
        host : 'mongodb://localhost:' + this.serverPort
    },
    linkedin : {
        id : '78xxzii3tophpt',
        secret : '4Iwpyp73hTH9grcj',
        callback : 'http://localhost:3000/auth/linkedin/callback'
    }
}

module.exports = config
