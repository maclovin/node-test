var express = require('express'),
    router = express.Router(),
    PassportLocal = require('../helpers/auth/local')

router.post('/',
    PassportLocal.authenticate('local'),
    function(req, res) {
        if (!req.user) {
            res.json({message : null, err : 'Fail'})

            return
        }

        res.json({message : req.user , err : null})
    }
)

module.exports = router
