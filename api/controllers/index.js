var express = require('express'),
    router = express.Router()

router.use('/login', require('./login'))
router.use('/user', require('./user'))
router.use('/logout', require('./logout'))
router.use('/auth', require('./auth'))
router.get('/', function(req, res) {
    console.log(req.user)
    res.json({user : req.user || null})
})

module.exports = router
