var express = require('express'),
    router = express.Router(),
    PassportLinkedin = require('../helpers/auth/linkedin')

router.get('/linkedin', PassportLinkedin.authenticate('linkedin'))

router.get('/linkedin/callback',
    PassportLinkedin.authenticate('linkedin', { failureRedirect: '/login', scope: ['r_basicprofile', 'r_emailaddress'] }),
    function(req, res) {
        res.json({message : req.user , err : null})
    }
)

module.exports = router
