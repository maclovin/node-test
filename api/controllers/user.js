var express = require('express'),
    router = express.Router(),
    joi = require('joi'),
    config = require('../config'),
    Hash = require('../helpers/security/hash'),
    Schemas = require('../helpers/validation/schemas'),
    User = require('../models/user')

router.get('/:id', function(req, res) {
    if (!req.user)
        res.redirect('/')

    res.json({view: 'User Info'})
})
router.post('/', function(req, res, next) {
    joi.validate({
        firstName : req.body.firstName,
        lastName: req.body.lastName,
        email : req.body.email,
        password : Hash.generate(req.body.password),
        company : req.body.company,
        category : req.body.category,
        address : req.body.address,
        city : req.body.city,
        state : req.body.state,
        zipcode : req.body.zipcode,
        phone : req.body.phone
    },
    Schemas.user,
    function(err, value) {
        if (!err) {
            User.findOne({email: value.email}, 'email id', function(err, person) {
                if (!person) {
                    var user = new User();
                    user.email = value.email
                    user.password = value.password
                    user.firstName = value.firstName
                    user.lastName = value.lasName
                    user.company = value.company
                    user.category = value.category
                    user.address = value.address
                    user.city = value.city
                    user.state = value.state
                    user.zipcode = value.zipcode
                    user.phone = value.phone
                    user.save(function(err, newUser) {
                        if (err) {
                            res.json({message : null, err : err})

                            return
                        }

                        res.json({message: 'OK', err: null})
                    })
                } else {
                    res.json({message : null, err : 'Already registered.'})

                    return
                }
            })
        }

    })
})


module.exports = router
